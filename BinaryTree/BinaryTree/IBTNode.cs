﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    public interface IBTNode<T>
    {
        public IBTNode<T> AddNode(T value);
        public void RemoveNode(T value);
        public void DepthFirstSearch();
        public void BreadthFirstSearch();

        public abstract void DeleteBinaryTree(ref IBTNode<T> node);
    }
}
