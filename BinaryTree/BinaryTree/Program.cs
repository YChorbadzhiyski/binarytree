﻿using System;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            IBTNode<int> firstNode = new BTNode<int>(1);
            IBTNode<int> head = firstNode;
            head.AddNode(4);
            head.AddNode(2);
            head.AddNode(12);
            head.AddNode(3);
            head.AddNode(77);
            head.AddNode(0);
            head.AddNode(7);
            System.Console.WriteLine();

            Console.WriteLine("Depths First Search Algorithm");
            head.DepthFirstSearch();
            Console.WriteLine();
            Console.WriteLine("Breadth First Search Algorithm");
            head.BreadthFirstSearch();

            var removeKey = 44;
            System.Console.WriteLine();
            head.RemoveNode(removeKey);
            System.Console.WriteLine($"\nDepth First Search Algorithm After (try of) Delition of node with key {removeKey}");
            head.DepthFirstSearch();
            System.Console.WriteLine($"\nBreadth First Search Algorithm After (try of) Delition of node with key {removeKey}");
            head.BreadthFirstSearch();
            Console.WriteLine();
            head.DeleteBinaryTree(ref head);
            Console.ReadLine();
        }
    }
}
