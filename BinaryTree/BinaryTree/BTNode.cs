﻿using System;
using System.Collections.Generic;

namespace BinaryTree
{
    /// <summary>
    /// Generic Binary Tree implementation 
    /// </summary>
    /// <typeparam name="T">Class param type</typeparam>
    public class BTNode<T> : IBTNode<T>
        where T: IComparable<T>
    {
        public BTNode()
        {
            Left = null;
            Right = null;
        }

        public BTNode(T value)
        {
            Data = value;
            Left = null;
            Right = null;
        }

        internal T Data { get; set; }
        internal BTNode<T> Left { get; set; }
        internal BTNode<T> Right { get; set; }

        /// <summary>
        /// Adds a node with a T key value. The order of addition is 
        /// left first, if not available add it on the right.
        /// </summary>
        /// <param name="value">The key value that will represent 
        /// the node's destinction</param>
        public IBTNode<T> AddNode(T value)
        {
            BTNode<T> newNode = new BTNode<T>(value);

            Queue<BTNode<T>> queue = new Queue<BTNode<T>>();
            queue.Enqueue(this);
            
            while(queue.Count > 0)
            {
                BTNode<T> current = queue.Dequeue();
                if (current.Left == null)
                {
                    current.Left = newNode;
                    Console.WriteLine($"Success! New node was added with value {value}");
                    break;
                }
                if (current.Right == null)
                {
                    current.Right = newNode;
                    Console.WriteLine($"Success! New node was added with value {value}");
                    break;
                }
                queue.Enqueue(current.Left);
                queue.Enqueue(current.Right);
            }

            return newNode;
        }
        

        /// <summary>
        /// This function removes a node based on the value passed in the parameter. 
        /// It checks if the value can be located and replaces it with rightmost 
        /// last node of the tree.
        /// </summary>
        /// <param name="value">The key value representign the node that should be deleted</param>
        public void RemoveNode(T value)
        {
            if(Data.Equals(value)){
                System.Console.WriteLine("Cannot delete root");
                return;
            }

            Queue<BTNode<T>> queue = new Queue<BTNode<T>>();
            BTNode<T> last  = GetLastNode();
            queue.Enqueue(this);
            
            while(queue.Count != 0)
            {
                BTNode<T> current = queue.Dequeue();
                
                if(current.Data.Equals(value)){
                    current.Data = GetLastNode().Data;
                    DeleteLastNode();
                    return;
                }
                else{
                    if(current.Left != null) queue.Enqueue(current.Left);
                    if(current.Right != null) queue.Enqueue(current.Right);
                }
            }
            System.Console.WriteLine($"\nNO NODE WITH VALUE {value} FOUND");
        }

        /// <summary>
        /// Implementation of the Depth First Algorithm which prints every element 
        /// in the tree in order (left, root, right)
        /// </summary>
        public void DepthFirstSearch()
        {
            if (CheckForNullValue())
            {
                Console.WriteLine("No tree passed. Please Provide a valid node.");
                return;
            }

            if (Left != null) Left.DepthFirstSearch();
            Console.Write(Data + " ");
            if (Right != null) Right.DepthFirstSearch();
        }

        /// <summary>
        /// Implementation of the Breadth First Algorithm which prints every element
        /// in the tree in order (root, left , right)
        /// </summary>
        public void BreadthFirstSearch()
        {
            if (CheckForNullValue())
            {
                Console.WriteLine("No tree passed. Please Provide a valid node.");
                return;
            }

            Queue<BTNode<T>> queue = new Queue<BTNode<T>>();
            queue.Enqueue(this);
           
            while(queue.Count > 0)
            {
                BTNode<T> current = queue.Dequeue();
                if (current.Left != null) queue.Enqueue(current.Left);
                if (current.Right != null) queue.Enqueue(current.Right);
                Console.Write(current.Data + " ");
            }
        }

        /// <summary>
        /// Deletes the whole tree structure
        /// </summary>
        /// <param name="node">Taking the reference of the root node<see cref="BTNode{T}"/></param>
        public void DeleteBinaryTree(ref IBTNode<T> node)
        {
            node = null;
            Console.WriteLine("\nTree is deleted");
        }

        /// <summary>
        /// Locates and returns the rightmost last node of the tree
        /// </summary>
        /// <returns>BTNode object of the last node<see cref="BTNode{T}"/></returns>
        private BTNode<T> GetLastNode()
        {
            Queue<BTNode<T>> queue = new Queue<BTNode<T>>();
            queue.Enqueue(this);
           
            while(queue.Count > 0)
            {
                BTNode<T> current = queue.Dequeue();
                if (current.Left != null) queue.Enqueue(current.Left);
                if (current.Right != null) queue.Enqueue(current.Right);

                if(queue.Count == 0) return current;
            }

            return null;
        }

        /// <summary>
        /// Locates and deletes the last  rightmost node of the tree
        /// </summary>
        private void DeleteLastNode()
        {
            Queue<BTNode<T>> queue = new Queue<BTNode<T>>();
            queue.Enqueue(this);
            BTNode<T> previousNode = null; 
            BTNode<T> presentNode = null;
           
            while(queue.Count > 0)
            {
                previousNode = presentNode;
                presentNode = queue.Dequeue();

                if (presentNode.Left == null) 
                {
                    previousNode.Right = null;
                    return;
                }

                if (presentNode.Right == null) 
                {
                    presentNode.Left = null;
                    return;
                }

                queue.Enqueue(presentNode.Left);
                queue.Enqueue(presentNode.Right);
            }
        }

        /// <summary>
        /// Checks if a null value object is passed
        /// </summary>
        /// <returns>False for not null value, True for null value</returns>
        private bool CheckForNullValue()
        {
            if (Data.Equals(null) && Left.Equals(null) && Right.Equals(null))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
